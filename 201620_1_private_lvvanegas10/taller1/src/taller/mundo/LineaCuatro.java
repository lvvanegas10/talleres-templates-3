package taller.mundo;

import java.util.ArrayList;
import java.util.Random;

public class LineaCuatro 
{
	/**
	 * Matriz que representa el estado actual del juego.
	 */
	private String[][] tablero;
	
	/**
	 * Lista que contiene a los jugadores
	 */
	private ArrayList<Jugador> jugadores;
	
	/**
	 * Nombre del jugador en turno
	 */
	private String atacante;
	
	/**
	 * Determina si el juego se terminó
	 */
	private boolean finJuego;
	
	/**
	 * El número del jugador en turno
	 */
	private int turno;
	/**
	 * El número del jugador en turno
	 */
	private int numFichas;
	
	/**
	 * Crea una nueva instancia del juego
	 */
	public LineaCuatro(ArrayList <Jugador> pJugadores,int pFil, int pCol, int pFichas)
	{
		tablero = new String[pFil][pCol];
		for(int i=0;i<tablero.length;i++)
		{
			for(int j=0;j<tablero[0].length;j++)
			{
				tablero[i][j]="___";
			}

		}
		jugadores = pJugadores;
		finJuego = false;
		turno = 0;
		atacante = jugadores.get(turno).darNombre();
		numFichas = pFichas;		
	}
	/**
	 * Retorna el tablero del juego
	 * @return tablero
	 */
	public String[][] darTablero()
	{
		return tablero;
	}
	
	/**
	 * Retorna el jugador en turno
	 * @return atacante jugador en turno
	 */
	public String darAtacante()
	{
		return atacante;
	}
	/**
	 * Determina si el juego termina
	 * @return true si el juego se termino, false de lo contrario
	 */
	public boolean fin()
	{
		return finJuego;
	}
	
	/**
	 * Registra una jugada aleatoria
	 */
	public void registrarJugadaAleatoria()
	{
		//TODO
		int columna = (int) (Math.random()* tablero[0].length);
		while(registrarJugada(columna)==false)		
			columna = (int) (Math.random()* tablero[0].length);				
	}
	
	/**
	 * Registra una jugada en el tablero
	 * @return true si la jugada se pudo realizar, false de lo contrario
	 */
	public boolean registrarJugada(int col)
	{
		//TODO
		int i =tablero.length - 1;
		while (tablero[i][col]!= "___" && i >=0)			
			i--;
		
		if(i>=0)
		{
			tablero[i][col] = " "+ jugadores.get(turno).darSimbolo() + " ";
			
			if(terminar(i, col)==false)
			{
				if(turno==1)
					turno=0;
				else
					turno=1;				
				
				atacante = jugadores.get(turno).darNombre();
			}			
			return true;
			
		}
		else
			return false;
	}
	
	/**
	 * Determina si una jugada causa el fin del juego
	 */
	public boolean terminar(int fil,int col)
	{
			
		int seguidas=1;
		if(seguidas < numFichas)
		{
			//Vertical
			seguidas=1;			
			for(int z =1; z+fil< tablero.length && tablero[fil+z][col].equals(tablero[fil][col]) ; z++)
			{
				seguidas++;				
			}
			for(int z =1; fil-z>=0 && tablero[fil-z][col].equals(tablero[fil][col]) ; z++)
			{
				seguidas++;
			}			
		}	
		if(seguidas < numFichas)
		{
			//horizontal
			seguidas=1;			
			for(int z =1; z+col< tablero[0].length && tablero[fil][col+z].equals(tablero[fil][col]) ; z++)
			{
				seguidas++;	
			}
			for(int z =1; col-z>=0 && tablero[fil][col-z].equals(tablero[fil][col]) ; z++)
			{
				seguidas++;
			}			
		}	
		if(seguidas < numFichas)
		{
			//Diagonal 1
			seguidas=1;
			for(int z =1; z+fil< tablero.length && z+col< tablero[0].length&& tablero[fil+z][col+z].equals(tablero[fil][col]) ; z++)
			{
				seguidas++;				
			}
			for(int z =1; fil-z>=0 && col-z>=0 && tablero[fil-z][col-z].equals(tablero[fil][col]) ; z++)
			{
				seguidas++;
			}			
		}	
		if(seguidas < numFichas)
		{
			//Diagonal 2
			seguidas=1;
			for(int z =1; z+fil< tablero.length && col-z>=0 && tablero[fil+z][col-z].equals(tablero[fil][col]) ; z++)
			{
				seguidas++;				
			}
			for(int z =1; fil-z>=0 && col+z< tablero[0].length && tablero[fil-z][col+z].equals(tablero[fil][col]) ; z++)
			{
				seguidas++;
			}					
		}	


		if(seguidas< numFichas)
			return false;
		else
		{
			finJuego =true;
			return true;
		}			
	}
}
