package taller.interfaz;

import java.util.ArrayList;
import java.util.Scanner;

import javax.swing.text.AttributeSet.CharacterAttribute;

import taller.mundo.Jugador;
import taller.mundo.LineaCuatro;

public class Interfaz 
{
	/**
	 * Juego actual
	 */
	private LineaCuatro juego;

	/**
	 * Escáner de texto ingresado por el usuario en la consola
	 */
	private Scanner sc;

	/**
	 * Crea una nueva instancia de la clase de interacción del usuario con la consola
	 */
	public Interfaz()
	{
		sc= new Scanner (System.in);
		iniciarJuego();
	}
	/**
	 * Imprime el menú principal
	 */
	public void imprimirMenu()
	{
		System.out.println("------------------------LÍNEA CUATRO------------------------");
		System.out.println("-Menú principal-");
		System.out.println("Ingrese un comando:");
		System.out.println("1. Empezar juego con amigos");
		System.out.println("2. Empezar juego vs máquina");
		System.out.println("3. Salir");		
	}

	/**
	 * Crea un nuevo juego
	 */
	public void empezarJuego() 
	{
		//TODO
		ArrayList<Jugador> lista = new ArrayList<>();
		Jugador nuevo1= nuevoJugador();
		Jugador nuevo2= nuevoJugador();
		
		System.out.println("-------------------------");
		System.out.println("Empezar juego co amigos");
		System.out.println("-------------------------");
		lista.add(nuevo1);
		lista.add(nuevo2);
		
		int fil=0;
		int col=0;
		int fic=0;
		while (true)
		{
			try{
				System.out.println("Ingrese el número de FILAS");
				fil= Integer.parseInt(sc.next());
				if (fil <4)
				{
					System.out.println("ERROR: Minimo 4 FILAS");
					continue;
				}						
				System.out.println("Ingrese el número de COLUMNAS");
				col= Integer.parseInt(sc.next());
				if(col<4)					
				{
					System.out.println("ERROR: Minimo 4 COLUMNAS");
					continue;
				}	
				System.out.println("Ingrese el número de FICHAS con el que gana");
				fic= Integer.parseInt(sc.next());
				if(fic > fil && fic >col)					
				{
					System.out.println("ERROR: Debe escoger menos FICHAS para ganar");
					continue;
				}	
				juego= new LineaCuatro(lista, fil, col, fic);	
				juego();
			}
			catch (Exception e)
			{
				System.out.println("Comando inválido");
				continue;
			}			
		}
	}

	/**
	 * Modera el juego entre jugadores
	 */
	public void juego()
	{
		//TODO
		imprimirTablero();
		
		while (true)
		{
			try
			{
				System.out.println(" ------------------------------- ");
				System.out.format("TURNO:  %s  ", juego.darAtacante());
				System.out.println("                                ");
				System.out.println("                                ");
				System.out.println(" ------------------------------- ");
				System.out.println("Ingrese la columna");
				System.out.println("------------------------------- ");
				
				String column = sc.next();
				int columna = Integer.parseInt(column);				

				if( juego.registrarJugada(columna)==false)
				{
					System.out.println("Jugada inválida. La columna esta llena");
					continue;
				}
				else
				{					
					imprimirTablero();
					if(juego.fin()==true)			
					{						
						System.out.format("El ganador es: %s", juego.darAtacante());
						System.out.println("-------------------------------------");
						System.out.println("Seleccione una opción");
						System.out.println("-------------------------------------");
						System.out.println("1. Nueva Partida");
						System.out.println("2. Salir");

						int op= sc.nextInt();
						if(op==1)
						{
							iniciarJuego();
						}
						else
						{
							System.out.format("¡Vuelva pronto!");
							break;
						}
					}
					else
					{
						System.out.println(" ------------------------------- ");
						System.out.format("TURNO:  %s  ", juego.darAtacante());
						System.out.println("                                ");
						System.out.println("                                ");
						System.out.println(" ------------------------------- ");
						System.out.println("Ingrese la columna");
						System.out.println("------------------------------- ");
						String col = sc.next();
						int colu = Integer.parseInt(col);				

						if( juego.registrarJugada(colu)==false)
						{
							System.out.println("Jugada inválida. La columna esta llena");
							continue;
						}
						imprimirTablero();
						if(juego.fin()==true)
						{
							System.out.format("El ganador es: %s", juego.darAtacante());
							System.out.println("-------------------------------------");
							System.out.println("Seleccione una opción");
							System.out.println("-------------------------------------");
							System.out.println("1. Nueva Partida");
							System.out.println("2. Salir");
						}
					}
				}

			}
			catch (Exception e)
			{
				System.out.println("Jugada inválida. Ingrese un número valido y escriba en la parte baja de la consola");
				continue;
			}
		}
	}
	/**
	 * Empieza el juego contra la máquina
	 */
	public void empezarJuegoMaquina()
	{
		ArrayList<Jugador> lista = new ArrayList<>();
		Jugador nuevo= nuevoJugador();
		Jugador maquina = new Jugador("Maquina", "°");
		System.out.println("-------------------------");
		System.out.println("Empezar juego vs máquina");
		System.out.println("-------------------------");
		lista.add(nuevo);
		lista.add(maquina);

		int fil=0;
		int col=0;
		int fic=0;
		while (true)
		{
			try{
				System.out.println("Ingrese el número de FILAS");
				fil= Integer.parseInt(sc.next());
				if (fil <4)
				{
					System.out.println("ERROR: Minimo 4 FILAS");
					continue;
				}						
				System.out.println("Ingrese el número de COLUMNAS");
				col= Integer.parseInt(sc.next());
				if(col<4)					
				{
					System.out.println("ERROR: Minimo 4 COLUMNAS");
					continue;
				}	
				System.out.println("Ingrese el número de FICHAS con el que gana");
				fic= Integer.parseInt(sc.next());
				if(fic > fil && fic >col)					
				{
					System.out.println("ERROR: Debe escoger menos FICHAS para ganar");
					continue;
				}	
				juego= new LineaCuatro(lista, fil, col, fic);	
				juegoMaquina();
			}
			catch (Exception e)
			{
				System.out.println("Comando inválido");
				continue;
			}			
		}
	}
	/**
	 * Modera el juego contra la máquina
	 */
	public void juegoMaquina()
	{
		imprimirTablero();
		System.out.println(" ------------------------------- ");
		System.out.format("TURNO:  %s  ", juego.darAtacante());
		System.out.println("                                ");
		System.out.println("                                ");

		while (true)
		{
			try
			{
				System.out.println(" ------------------------------- ");
				System.out.println("Ingrese la columna");
				System.out.println(" ------------------------------- ");
				String column = sc.next();
				int columna = Integer.parseInt(column);				

				if( juego.registrarJugada(columna)==false)
				{
					System.out.println("Jugada inválida. La columna esta llena");
					continue;
				}
				else
				{					
					imprimirTablero();
					if(juego.fin()==true)			
					{						
						System.out.format("El ganador es: %s", juego.darAtacante());
						System.out.println("-------------------------------------");
						System.out.println("Seleccione una opción");
						System.out.println("-------------------------------------");
						System.out.println("1. Nueva Partida");
						System.out.println("2. Salir");

						int op= sc.nextInt();
						if(op==1)
						{
							iniciarJuego();
						}
						else
						{
							System.out.format("¡Vuelva pronto!");
							break;
						}
					}
					else
					{
						juego.registrarJugadaAleatoria();
						imprimirTablero();
						if(juego.fin()==true)
						{
							System.out.format("El ganador es: %s", juego.darAtacante());
							System.out.println("-------------------------------------");
							System.out.println("Seleccione una opción");
							System.out.println("-------------------------------------");
							System.out.println("1. Nueva Partida");
							System.out.println("2. Salir");
						}
					}
				}

			}
			catch (Exception e)
			{
				System.out.println("Jugada inválida. Ingrese un número valido y escriba en la parte baja de la consola");
				continue;
			}
		}
	}
	/**
	 * Inicializa un jugador
	 */
	public Jugador nuevoJugador()
	{
		System.out.println("-Empezar juego-");
		System.out.println("Ingrese el nombre del Jugador");
		String nombre1=sc.next();

		System.out.println("Ingrese el simbolo con el que desea jugar");		

		while (true)
		{
			try
			{
				String a = sc.next();
				if (a.length()==1 && (Character.isLetter(a.charAt(0)) || Character.isDigit(a.charAt(0))))
				{
					return new Jugador(nombre1, a);
				}					
				else
				{
					System.out.println("Comando inválido: Ingrese el simbolo con el que desea jugar, debe ser un digito o una letra");
					continue;
				}
			}
			catch (NumberFormatException e)
			{
				System.out.println("Comando inválido: Ingrese el simbolo con el que desea jugar");
				continue;
			}
		}
	}
	/**
	 * Imprime el estado actual del juego
	 */
	public void imprimirTablero()
	{
		//TODO
		String matriz [][]= juego.darTablero();
		System.out.print("   ");
		for (int j =0; j< matriz[0].length;j++)
		{								
			System.out.print("  "+ j+" ");			
		}		

		for (int i =0; i< matriz.length;i++)
		{		
			System.out.println(" ");			
			System.out.print(i+ "   ");			
			for (int j =0; j< matriz[0].length;j++)
			{									
				System.out.print(matriz[i][j] + " ");
			}
		}	
		System.out.println("   ");
	}
	/**
	 * Iniciar
	 */
	public void iniciarJuego()
	{
		while (true)
		{
			imprimirMenu();
			try
			{
				int opt=Integer.parseInt(sc.next());
				if(opt==1)
				{
					empezarJuego();
				}
				else if(opt==2)
				{
					empezarJuegoMaquina();
				}
				else if(opt==3)
				{
					System.out.println("¡Vuelva pronto!");
					break;
				}
				else
				{
					System.out.println("Comando inválido");
					continue;
				}
			}
			catch (NumberFormatException e)
			{
				System.out.println("Comando inválido");
				continue;
			}
		}
	}
}
